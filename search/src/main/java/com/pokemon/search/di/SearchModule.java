package com.pokemon.search.di;

import dagger.Module;

@Module
abstract class SearchModule {


//    @Provides
//    static HttpLoggingInterceptor getLoggingInterceptor() {
//        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
//        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        return loggingInterceptor;
//    }
//
//    @Provides
//    static OkHttpClient provideOkHttpClient(HttpLoggingInterceptor loggingInterceptor) {
//        OkHttpClient.Builder builder = new OkHttpClient.Builder()
//                .addInterceptor(loggingInterceptor)
//                .connectTimeout(60, TimeUnit.SECONDS)
//                .readTimeout(60, TimeUnit.SECONDS)
//                .writeTimeout(60, TimeUnit.SECONDS);
//
//        return builder.build();
//    }
//
//    @Provides
//    static Retrofit.Builder provideRetrofitBuilder(OkHttpClient okHttpClient) {
//        return new Retrofit.Builder()
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                .addConverterFactory(GsonConverterFactory.create())
//                .client(okHttpClient);
//    }
//
//    @Provides
//    static Retrofit provideRetrofit(Retrofit.Builder retrofitBuilder) {
//        return retrofitBuilder.baseUrl("https://pokeapi.co/api/v2/").build();
//    }
//
//    @Provides
//    static PokemonService providePokemonService(Retrofit retrofit) {
//        return retrofit.create(PokemonService.class);
//    }
//
//    @Provides
//    static PokemonRepository providePokemonRepository(PokemonRepositoryImpl pokemonRepositoryImpl) {
//        return pokemonRepositoryImpl;
//    }
}
