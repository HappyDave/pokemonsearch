package com.pokemon.search.di;

import com.pokemon.core.di.action.ShowSearchAction;
import com.pokemon.search.action.ShowSearchActionImpl;

import dagger.Module;
import dagger.Provides;

@Module
final class SearchExportModule {
    @Provides
    static ShowSearchAction provideShowSearchAction() {
        return new ShowSearchActionImpl();
    }
}
