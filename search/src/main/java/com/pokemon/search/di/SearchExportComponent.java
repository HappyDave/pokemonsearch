package com.pokemon.search.di;

import com.pokemon.core.di.expose.SearchProvider;

import dagger.Component;

@Component(modules = SearchExportModule.class)
public interface SearchExportComponent extends SearchProvider {
    final class Initializer {
        public static SearchExportComponent init() {
            return DaggerSearchExportComponent.builder()
                    .build();
        }

        private Initializer() {
        }
    }
}
