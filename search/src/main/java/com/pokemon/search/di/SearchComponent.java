package com.pokemon.search.di;

import com.pokemon.core.di.expose.ApplicationProvider;
import com.pokemon.core.di.scope.FeatureScope;
import com.pokemon.search.presentation.SearchActivity;
import com.pokemon.search.presentation.ViewModelModule;

import dagger.Component;

@FeatureScope
@Component(modules = {SearchModule.class, ViewModelModule.class},
        dependencies = ApplicationProvider.class)
public interface SearchComponent {
    void inject(SearchActivity searchActivity);

    final class Initializer {
        public static SearchComponent init(final ApplicationProvider appComponent) {
            return DaggerSearchComponent.builder()
                    .applicationProvider(appComponent)
                    .build();
        }

        private Initializer() {
        }
    }
}
