package com.pokemon.search.domain;

import android.support.annotation.NonNull;

import com.pokemon.core.domain.Pokemon;
import com.pokemon.core.domain.PokemonRepository;
import com.pokemon.core.domain.UseCase;

import javax.inject.Inject;

import io.reactivex.Single;

public class GetPokemon implements UseCase.Request<GetPokemon.Params, Pokemon> {

    @NonNull
    private final PokemonRepository pokemonRepository;

    @Inject
    public GetPokemon(@NonNull final PokemonRepository pokemonRepository) {
        this.pokemonRepository = pokemonRepository;
    }

    @NonNull
    @Override
    public Single<Pokemon> getSingle(@NonNull Params params) {
        return pokemonRepository.fetchPokemon(params.name);
    }


    public static final class Params {
        private final String name;

        Params(String name) {
            this.name = name;
        }

        public static Params forPokemon(String name) {
            return new Params(name);
        }
    }
}
