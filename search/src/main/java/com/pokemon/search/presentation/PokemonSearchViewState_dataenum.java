package com.pokemon.search.presentation;

import com.spotify.dataenum.DataEnum;
import com.spotify.dataenum.dataenum_case;

@DataEnum
interface PokemonSearchViewState_dataenum {
    dataenum_case Success(PokemonViewEntity pokemon);
}
