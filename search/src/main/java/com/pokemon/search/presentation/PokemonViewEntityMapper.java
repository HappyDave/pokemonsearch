package com.pokemon.search.presentation;


import com.pokemon.core.domain.Pokemon;

import io.reactivex.functions.Function;

class PokemonViewEntityMapper implements Function<Pokemon, PokemonViewEntity> {
    @Override
    public PokemonViewEntity apply(Pokemon pokemon) {
        return PokemonViewEntity.builder()
                .id(String.valueOf(pokemon.id()))
                .name(pokemon.name())
                .height(String.valueOf(pokemon.height()))
                .weight(String.valueOf(pokemon.weight()))
                .sprite(pokemon.sprites().frontDefault())
                .build();
    }
}
