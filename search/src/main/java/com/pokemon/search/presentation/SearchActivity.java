package com.pokemon.search.presentation;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.jakewharton.rxbinding3.widget.RxTextView;
import com.jakewharton.rxrelay2.PublishRelay;
import com.pokemon.core.App;
import com.pokemon.search.R;
import com.pokemon.search.databinding.ActivitySearchBinding;
import com.pokemon.search.di.SearchComponent;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class SearchActivity extends AppCompatActivity {

    @Inject
    ViewModelFactory viewModelFactory;

    @NonNull
    private final CompositeDisposable disposables = new CompositeDisposable();

    private PokemonSearchViewModel viewModel;
    private ActivitySearchBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        inject();
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search);

        PublishRelay<PokemonSearchViewAction> actionStream = PublishRelay.create();
        disposables.add(RxTextView.textChanges(binding.inputName)
                .debounce(500, TimeUnit.MILLISECONDS)
                .filter(text -> text.length() > 0)
                .subscribe(charSequence ->
                        actionStream.accept(PokemonSearchViewAction.search(charSequence))));

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(PokemonSearchViewModel.class);
        viewModel.getState().observe(this, this::handleState);
        viewModel.attach(actionStream);
    }

    private void handleState(PokemonSearchViewState state) {
        state.match(
                success -> binding.setPokemon(success.pokemon()));
    }

    private void inject() {
        SearchComponent.Initializer
                .init(((App) getApplication()).getAppComponent())
                .inject(this);
    }

    @Override
    protected void onStop() {
        disposables.clear();
        viewModel.detach();
        super.onStop();
    }
}
