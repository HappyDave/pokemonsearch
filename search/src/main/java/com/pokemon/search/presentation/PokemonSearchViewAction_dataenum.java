package com.pokemon.search.presentation;

import com.spotify.dataenum.DataEnum;
import com.spotify.dataenum.dataenum_case;

@DataEnum
interface PokemonSearchViewAction_dataenum {
    dataenum_case Search(CharSequence text);
}
