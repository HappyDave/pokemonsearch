package com.pokemon.search.presentation;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import com.pokemon.search.domain.GetPokemon;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;

class PokemonSearchViewModel extends ViewModel {

    @NonNull
    private final GetPokemon getPokemon;
    @NonNull
    private final CompositeDisposable disposables = new CompositeDisposable();
    @NonNull
    private final MutableLiveData<PokemonSearchViewState> stateStream = new MutableLiveData<>();
    @NonNull
    private final PokemonViewEntityMapper pokemonViewEntityMapper;

    public PokemonSearchViewModel(@NonNull final GetPokemon getPokemon) {
        this.getPokemon = getPokemon;
        pokemonViewEntityMapper = new PokemonViewEntityMapper();
    }

    public void attach(Observable<PokemonSearchViewAction> actionStream) {
        disposables.add(actionStream
                .switchMapCompletable(this::handleAction)
                .subscribe());
    }

    private Completable handleAction(PokemonSearchViewAction action) {
        return action.map(this::searchPokemon);
    }

    @NonNull
    private Completable searchPokemon(PokemonSearchViewAction.Search action) {
        return getPokemon.getSingle(GetPokemon.Params.forPokemon(action.text().toString()))
                .map(pokemonViewEntityMapper)
                .doOnSuccess(it -> stateStream.postValue(PokemonSearchViewState.success(it)))
                .ignoreElement();
    }

    void detach() {
        disposables.clear();
    }

    LiveData<PokemonSearchViewState> getState() {
        return stateStream;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposables.clear();
    }
}
