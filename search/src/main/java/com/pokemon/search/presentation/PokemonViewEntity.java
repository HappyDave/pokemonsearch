package com.pokemon.search.presentation;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class PokemonViewEntity {
    public abstract String id();

    @NonNull
    public abstract String name();

    public abstract String weight();

    public abstract String height();

    @NonNull
    public abstract String sprite();

    @NonNull
    public static Builder builder() {
        return new AutoValue_PokemonViewEntity.Builder();
    }

    @AutoValue.Builder
    public interface Builder {

        Builder id(final String id);

        Builder name(final String name);

        Builder weight(final String weight);

        Builder height(final String height);

        Builder sprite(final String sprite);

        PokemonViewEntity build();
    }
}
