package com.pokemon.search.action;

import android.content.Context;
import android.content.Intent;

import com.pokemon.core.di.action.ShowSearchAction;
import com.pokemon.search.presentation.SearchActivity;

public class ShowSearchActionImpl implements ShowSearchAction {

    @Override
    public void run(Context context) {
        context.startActivity(new Intent(context, SearchActivity.class));
    }
}
