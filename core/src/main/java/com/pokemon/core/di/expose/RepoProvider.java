package com.pokemon.core.di.expose;

import com.pokemon.core.domain.PokemonRepository;

public interface RepoProvider {
    PokemonRepository providePokemonRepository();
}
