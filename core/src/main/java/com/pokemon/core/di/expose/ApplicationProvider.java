package com.pokemon.core.di.expose;

public interface ApplicationProvider extends
        MainProvider,
        RepoProvider,
        SearchProvider {

}
