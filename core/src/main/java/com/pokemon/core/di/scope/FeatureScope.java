package com.pokemon.core.di.scope;

import javax.inject.Scope;

@Scope
public @interface FeatureScope {
}
