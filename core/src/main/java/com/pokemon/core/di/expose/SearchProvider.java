package com.pokemon.core.di.expose;

import com.pokemon.core.di.action.ShowSearchAction;

public interface SearchProvider {
    ShowSearchAction provideShowSearchAction();
}
