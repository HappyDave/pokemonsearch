package com.pokemon.core.di.action;

import android.content.Context;

public interface ShowSearchAction {
    void run(Context context);
}
