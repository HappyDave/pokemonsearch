package com.pokemon.core.di.expose;

import com.pokemon.core.App;

public interface MainProvider {
    App provideContext();
}
