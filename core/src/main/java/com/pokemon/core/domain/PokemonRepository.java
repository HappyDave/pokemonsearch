package com.pokemon.core.domain;

import android.support.annotation.NonNull;

import io.reactivex.Single;

public interface PokemonRepository {
    @NonNull
    Single<Pokemon> fetchPokemon(String name);
}
