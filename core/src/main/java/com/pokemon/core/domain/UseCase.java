package com.pokemon.core.domain;

import android.support.annotation.NonNull;

import io.reactivex.Single;

public interface UseCase {

    interface Request<Params, Result> extends UseCase {

        @NonNull
        Single<Result> getSingle(@NonNull final Params params);
    }
}
