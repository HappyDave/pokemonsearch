package com.pokemon.core.domain;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class PokemonSprites {
    @NonNull
    public abstract String frontDefault();

    @NonNull
    public abstract String frontShiny();

    @NonNull
    public abstract String backDefault();

    @NonNull
    public abstract String backShiny();

    @NonNull
    public static Builder builder() {
        return new AutoValue_PokemonSprites.Builder();
    }

    @AutoValue.Builder
    public interface Builder {

        Builder frontDefault(final String frontDefault);

        Builder frontShiny(final String frontShiny);

        Builder backDefault(final String backDefault);

        Builder backShiny(final String backShiny);

        PokemonSprites build();
    }
}
