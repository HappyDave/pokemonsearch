package com.pokemon.core.domain;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class Pokemon {

    public abstract int id();

    @NonNull
    public abstract String name();

    public abstract int weight();

    public abstract int height();

    @NonNull
    public abstract PokemonSprites sprites();

    @NonNull
    public static Builder builder() {
        return new AutoValue_Pokemon.Builder();
    }

    @AutoValue.Builder
    public interface Builder {

        Builder id(final int id);

        Builder name(final String name);

        Builder weight(final int weight);

        Builder height(final int height);

        Builder sprites(final PokemonSprites sprites);

        Pokemon build();
    }
}
