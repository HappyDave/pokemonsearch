package com.pokemon.core;

import com.pokemon.core.di.expose.ApplicationProvider;

public interface App {
    ApplicationProvider getAppComponent();
}
