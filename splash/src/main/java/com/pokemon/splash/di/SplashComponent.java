package com.pokemon.splash.di;

import com.pokemon.core.di.expose.ApplicationProvider;
import com.pokemon.core.di.scope.FeatureScope;
import com.pokemon.splash.presentation.SplashActivity;

import dagger.Component;

@FeatureScope
@Component(dependencies = ApplicationProvider.class)
public interface SplashComponent {
    void inject(SplashActivity splashActivity);

    final class Initializer {
        public static SplashComponent init(final ApplicationProvider appComponent) {
            return DaggerSplashComponent.builder()
                    .applicationProvider(appComponent)
                    .build();
        }

        private Initializer() {
        }
    }
}
