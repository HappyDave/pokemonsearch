package com.pokemon.splash.presentation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.m.splash.R;
import com.pokemon.core.App;
import com.pokemon.core.di.action.ShowSearchAction;
import com.pokemon.splash.di.SplashComponent;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Completable;

public class SplashActivity extends AppCompatActivity {

    @Inject
    ShowSearchAction showSearchAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        inject();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Completable.timer(2, TimeUnit.SECONDS)
                .subscribe(this::goToSearch);
    }

    private void inject() {
        SplashComponent.Initializer
                .init(((App) getApplication()).getAppComponent())
                .inject(this);
    }

    private void goToSearch() {
        showSearchAction.run(this);
    }
}
