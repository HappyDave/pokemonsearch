package com.repository.repository.data;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface PokemonService {

    @GET("pokemon/{name}")
    Single<PokemonRaw> getPokemon(@Path("name") String name);
}
