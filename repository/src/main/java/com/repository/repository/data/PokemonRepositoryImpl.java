package com.repository.repository.data;

import android.support.annotation.NonNull;

import com.pokemon.core.domain.Pokemon;
import com.pokemon.core.domain.PokemonRepository;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class PokemonRepositoryImpl implements PokemonRepository {

    @NonNull
    private final PokemonService pokemonService;
    @NonNull
    private final PokemonMapper pokemonMapper;

    @Inject
    public PokemonRepositoryImpl(@NonNull final PokemonService pokemonService) {
        this.pokemonService = pokemonService;
        //TODO create cache (store?)
        pokemonMapper = new PokemonMapper();
    }

    @NonNull
    @Override
    public Single<Pokemon> fetchPokemon(String name) {
        return pokemonService.getPokemon(name)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                //TODO .onErrorResumeNext(Single.never())
                .map(pokemonMapper);
    }
}
