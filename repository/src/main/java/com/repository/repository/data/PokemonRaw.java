package com.repository.repository.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class PokemonRaw {
    @Expose
    @SerializedName("sprites")
    private Sprites sprites;
    @Expose
    @SerializedName("weight")
    private int weight;
    @Expose
    @SerializedName("height")
    private int height;
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("id")
    private int id;

    Sprites getSprites() {
        return sprites;
    }

    int getWeight() {
        return weight;
    }

    int getHeight() {
        return height;
    }

    String getName() {
        return name;
    }

    int getId() {
        return id;
    }

    static class Sprites {
        @Expose
        @SerializedName("front_shiny")
        private String frontShiny;
        @Expose
        @SerializedName("front_default")
        private String frontDefault;
        @Expose
        @SerializedName("back_shiny")
        private String backShiny;
        @Expose
        @SerializedName("back_default")
        private String backDefault;

        String getFrontShiny() {
            return frontShiny;
        }

        String getFrontDefault() {
            return frontDefault;
        }

        String getBackShiny() {
            return backShiny;
        }

        String getBackDefault() {
            return backDefault;
        }
    }
}
