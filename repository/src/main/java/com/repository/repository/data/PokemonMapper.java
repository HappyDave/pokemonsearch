package com.repository.repository.data;

import com.pokemon.core.domain.Pokemon;

import io.reactivex.functions.Function;

class PokemonMapper implements Function<PokemonRaw, Pokemon> {
    @Override
    public Pokemon apply(PokemonRaw pokemonRaw) {
        //TODO assertEssentialParams(raw);

        return Pokemon.builder()
                .id(pokemonRaw.getId())
                .name(pokemonRaw.getName())
                .height(pokemonRaw.getHeight())
                .weight(pokemonRaw.getWeight())
                .sprites(PokemonSpritesMapper.processRaw(pokemonRaw.getSprites()))
                .build();
    }
}
