package com.repository.repository.data;

import android.support.annotation.NonNull;

import com.pokemon.core.domain.PokemonSprites;

final class PokemonSpritesMapper {

    private PokemonSpritesMapper() {
    }

    @NonNull
    static PokemonSprites processRaw(PokemonRaw.Sprites spritesRaw) {
        //TODO assertEssentialParams(raw);

        return PokemonSprites.builder()
                .frontDefault(spritesRaw.getFrontDefault())
                .frontShiny(spritesRaw.getFrontShiny())
                .backDefault(spritesRaw.getBackDefault())
                .backShiny(spritesRaw.getBackShiny())
                .build();
    }
}
