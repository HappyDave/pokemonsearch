package com.repository.repository.di;

import com.pokemon.core.domain.PokemonRepository;
import com.repository.repository.data.PokemonRepositoryImpl;
import com.repository.repository.data.PokemonService;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
final class RepoModule {
    @Provides
    static PokemonService provideRetrofit(Retrofit.Builder retrofitBuilder) {
        Retrofit retrofit = retrofitBuilder
                .baseUrl("https://pokeapi.co/api/v2/")
                .build();
        return retrofit.create(PokemonService.class);
    }

    @Provides
    static PokemonRepository provideRepository(PokemonRepositoryImpl pokemonRepository) {
        return pokemonRepository;
    }
}
