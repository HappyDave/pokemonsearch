package com.repository.repository.di;

import com.pokemon.core.di.expose.RepoProvider;
import com.pokemon.network.NetworkProvider;
import com.pokemon.network.di.DaggerNetworkComponent;
import com.pokemon.network.di.NetworkComponent;

import dagger.Component;

@Component(modules = RepoModule.class,
        dependencies = NetworkProvider.class)
public interface RepoComponent extends RepoProvider {

    final class Initializer {
        public static RepoComponent init() {
            NetworkComponent networkComponent = DaggerNetworkComponent.builder()
                    .build();
            return DaggerRepoComponent.builder()
                    .networkProvider(networkComponent)
                    .build();
        }

        private Initializer() {
        }
    }
}
