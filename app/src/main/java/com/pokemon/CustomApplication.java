package com.pokemon;

import android.app.Application;

import com.pokemon.core.App;
import com.pokemon.core.di.expose.ApplicationProvider;
import com.pokemon.di.AppComponent;

public class CustomApplication extends Application implements App {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = AppComponent.Initializer.init(this);
    }

    @Override
    public ApplicationProvider getAppComponent() {
        return appComponent;
    }
}
