package com.pokemon.di;

import com.pokemon.core.App;
import com.pokemon.core.di.expose.MainProvider;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@Singleton
@Component(modules = MainModule.class)
public interface MainComponent extends MainProvider {

    @Component.Builder
    interface Builder {
        MainComponent build();

        @BindsInstance
        Builder app(App app);
    }

    final class Initializer {
        static MainComponent init(final App app) {
            return DaggerMainComponent.builder()
                    .app(app)
                    .build();
        }

        private Initializer() {
        }
    }
}
