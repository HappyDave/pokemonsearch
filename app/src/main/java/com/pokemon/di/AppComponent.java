package com.pokemon.di;

import com.pokemon.CustomApplication;
import com.pokemon.core.di.expose.ApplicationProvider;
import com.pokemon.core.di.expose.MainProvider;
import com.pokemon.core.di.expose.RepoProvider;
import com.pokemon.search.di.SearchExportComponent;
import com.repository.repository.di.RepoComponent;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(dependencies = {
        MainProvider.class,
        RepoProvider.class,
        SearchExportComponent.class
})
public interface AppComponent extends ApplicationProvider {

    final class Initializer {

        public static AppComponent init(final CustomApplication app) {
            MainComponent mainComponent = MainComponent.Initializer
                    .init(app);

            SearchExportComponent searchExportComponent = SearchExportComponent.Initializer
                    .init();

            RepoComponent repoComponent = RepoComponent.Initializer
                    .init();

            return DaggerAppComponent.builder()
                    .mainProvider(mainComponent)
                    .repoProvider(repoComponent)
                    .searchExportComponent(searchExportComponent)
                    .build();
        }

        private Initializer() {
        }
    }
}
