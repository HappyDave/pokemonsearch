package com.pokemon.network;

import retrofit2.Retrofit;

public interface NetworkProvider {
    Retrofit.Builder provideNetworkClient();
}
