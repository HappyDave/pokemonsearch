package com.pokemon.network.di;

import com.pokemon.network.NetworkProvider;

import dagger.Component;

@Component(modules = NetworkModule.class)
public interface NetworkComponent extends NetworkProvider {
}
